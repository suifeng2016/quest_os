
#include <kern/console.h>
#include <inc/stdio.h>
#include <inc/string.h>


void i386_init(void)
{
	extern char edata[], end[];
	memset(edata, 0, end - edata);
	cons_init();

	cprintf("welcome to quest OS\n");
	while (1);
}