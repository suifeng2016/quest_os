//
// Created by 86134 on 2023/6/3.
//

#ifndef DAY2_TYPE_H
#define DAY2_TYPE_H


typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef short int16_8;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
typedef long long int64_t;
typedef unsigned long long uint64_t;

typedef uint32_t pte_t;
typedef uint32_t pde_t;

typedef uint32_t uintptr_t;
typedef uint32_t size_t;


typedef _Bool bool;
enum { false, true };



#endif //DAY2_TYPE_H
