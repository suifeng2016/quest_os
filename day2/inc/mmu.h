#define BOOTSECT_ADDR   0x7c00
#define STA_X      0x8        // Executable segment
#define STA_E     0x4        // Expand down (non-executable segments)
#define STA_C     0x4        // Conforming code segment (executable only)
#define STA_W     0x2        // Writeable (non-executable segments)
#define STA_R     0x2        // Readable (executable segments)
#define STA_A     0x1        // Accessed

#define SEG_NULL                   \
   .word 0, 0;                   \
   .byte 0, 0, 0, 0
#define SEG(type,base,lim)             \
   .word (((lim) >> 12) & 0xffff), ((base) & 0xffff); \
   .byte (((base) >> 16) & 0xff), (0x90 | (type)),       \
      (0xC0 | (((lim) >> 28) & 0xf)), (((base) >> 24) & 0xff)

#define	KERNBASE	0xF0000000
#define	KERNBASE_P	0x100000


#define NPDENTRIES	1024	
#define NPTENTRIES	1024		

#define PGSIZE		4096
#define KSTKSIZE	(8*PGSIZE)

#define PTXSHIFT	12		// offset of PTX in a linear address
#define PDXSHIFT	22		// offset of PDX in a linear address


#define PTE_P		0x001	// Present
#define PTE_W		0x002	// Writeable
#define PTE_U		0x004	// User

#define CR0_PE		0x00000001	// Protection Enable
#define CR0_MP		0x00000002	// Monitor coProcessor
#define CR0_EM		0x00000004	// Emulation
#define CR0_TS		0x00000008	// Task Switched
#define CR0_ET		0x00000010	// Extension Type
#define CR0_NE		0x00000020	// Numeric Errror
#define CR0_WP		0x00010000	// Write Protect
#define CR0_AM		0x00040000	// Alignment Mask
#define CR0_NW		0x20000000	// Not Writethrough
#define CR0_CD		0x40000000	// Cache Disable
#define CR0_PG		0x80000000	// Paging

#define CR4_PCE		0x00000100	// Performance counter enable
#define CR4_MCE		0x00000040	// Machine Check Enable
#define CR4_PSE		0x00000010	// Page Size Extensions
#define CR4_DE		   0x00000008	// Debugging Extensions
#define CR4_TSD		0x00000004	// Time Stamp Disable
#define CR4_PVI		0x00000002	// Protected-Mode Virtual Interrupts
#define CR4_VME		0x00000001	// V86 Mode Extensions

#define HELLO		hello	// test
